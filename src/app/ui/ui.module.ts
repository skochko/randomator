import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from "@angular/flex-layout";
import { RouterModule } from '@angular/router';
import { AllProductsComponent } from './all-products/all-products.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { HistoryComponent } from './history/history.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    MainNavComponent,
    AllUsersComponent,
    HistoryComponent,
    AllProductsComponent,
    UserLoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    MainNavComponent
  ]
})
export class UiModule { 
}