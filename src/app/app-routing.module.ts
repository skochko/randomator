import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllProductsComponent } from './ui/all-products/all-products.component';
import { AllUsersComponent } from './ui/all-users/all-users.component';
import { HistoryComponent } from './ui/history/history.component';
import { UserLoginComponent } from './ui/user-login/user-login.component';
const routes: Routes = [
  { path: 'all-products', component: AllProductsComponent },
  { path: 'all-users', component: AllUsersComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'login', component: UserLoginComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }